import { Breadcrumbs, Breadcrumb } from '../breadcrumb-config';

// Rich snippet Breadcrumb - item
export const getRsBreadcrumb = (crumb: Breadcrumb, number: number) => {
  return {
    '@type': 'ListItem',
    'position': number.toString(),
    'item': {
      '@id': crumb.fullUrl,
      'name': crumb.label
    }
  };
};

// Rich snippet Breadcrumb - list
export const getRsBreadcrumbs = (crumbs: Breadcrumbs) => {
  return {
    '@context': 'http://schema.org',
    '@type': 'BreadcrumbList',
    'itemListElement': crumbs.map((crumb, index) => getRsBreadcrumb(crumb, index + 1))
  };
};
