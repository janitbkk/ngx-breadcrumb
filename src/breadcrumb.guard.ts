import { Injectable } from '@angular/core';
import { BreadcrumbService } from './breadcrumb.service';

import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class BreadcrumbGuard implements CanActivate {

	public constructor(private bc: BreadcrumbService) {}

	public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		this.bc.routeChange(route);
		return true;
	}
}
