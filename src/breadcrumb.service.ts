import { Injectable, Inject, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { ActivatedRouteSnapshot } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/distinctUntilChanged';

import {
  BREADCRUMB_CONFIG,
  Breadcrumbs,
  Breadcrumb,
  BreadcrumbConfig,
  defaultConfig
} from './breadcrumb-config';
import { getRsBreadcrumbs } from './helpers/index';

@Injectable()
export class BreadcrumbService {
  private _current: BehaviorSubject<Breadcrumbs> = new BehaviorSubject<Breadcrumbs>([]);
  readonly current: Observable<Breadcrumbs> = this._current.asObservable().distinctUntilChanged();

  private scriptId = 'breadcrumb_ld';
  private renderer: Renderer2;

  constructor(
    @Inject(BREADCRUMB_CONFIG) private config: BreadcrumbConfig,
    @Inject(DOCUMENT) private doc,
  ) {
    this.config = { ...defaultConfig, ...config };
  }

  public routeChange(routeSnapshot) {
    const crumbs = this.getBreadcrumbs(routeSnapshot);
    this._current.next(crumbs);
    this.appendJsonLd(crumbs);
  }

  public init(renderer: Renderer2) {
    this.renderer = renderer;
  }

  private appendJsonLd(crumbs) {
    this.removeScript();
    this.addScript(crumbs);
  }

  private removeScript() {
    const element = this.doc.querySelector(`#${this.scriptId}`);
    if (element) {
      this.renderer.parentNode(element).removeChild(element);
    }
  }

  private addScript(crumbs) {
    const script = this.renderer.createElement('script');
    script.id = this.scriptId;
    script.type = `application/ld+json`;
    script.text = `${JSON.stringify(getRsBreadcrumbs(crumbs))}`;
    this.renderer.appendChild(this.doc['body'], script);
  }

  private getBreadcrumbs(routeSnapshot: ActivatedRouteSnapshot): Breadcrumbs {
    let urlEndpoints = [];
    let breadcrumbs = this.initBreadcrumbs();

    routeSnapshot.pathFromRoot
      .filter(route => route.url.length > 0)
      .forEach(route => {
        const urlEndpoint = route.url[0].path;
        urlEndpoints = [ ...urlEndpoints, urlEndpoint];
        breadcrumbs = [ ...breadcrumbs, this.createBreadcrumb(route, urlEndpoint, urlEndpoints)];
      });

    return breadcrumbs;
  }

  private initBreadcrumbs(): Breadcrumbs {
    return this.config.homeRoute
      ? [this.createBreadcrumb(this.config.homeRoute, '', [])]
      : [];
  }

  private createBreadcrumb(route: any, url: string, urls: string[]): Breadcrumb {
    return {
      label: this.routePropertyExists(route) ? route.data[this.config.routeProperty].label : '',
      params: route.params,
      url: url,
      fullUrl: this.config.baseUrl + '/' + urls.toString().replace(',', '/')
    };
  }

  private routePropertyExists(route): boolean {
    return route.data && route.data[this.config.routeProperty];
  }
}
