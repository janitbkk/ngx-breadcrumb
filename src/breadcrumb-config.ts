import { InjectionToken } from '@angular/core';
import { Params, Route } from '@angular/router';

export const BREADCRUMB_CONFIG = new InjectionToken('Breadcrumb Config');

export interface BreadcrumbConfig {
  baseUrl: string;
  homeRoute?: Route;
  routeProperty?: string;
}

export const defaultConfig: BreadcrumbConfig = {
  baseUrl: '',
  homeRoute: null,
  routeProperty: 'breadcrumb'
};

export interface BreadcrumbRouteConfig {
  label: string;
}

export interface Breadcrumb {
  label: string;
  params: Params;
  url: string;
  fullUrl: string;
}

export type Breadcrumbs = Breadcrumb[];
