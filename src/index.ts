import { NgModule, ModuleWithProviders, Optional, SkipSelf } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BreadcrumbService } from './breadcrumb.service';
import { BreadcrumbGuard } from './breadcrumb.guard';
import { BREADCRUMB_CONFIG, BreadcrumbConfig } from './breadcrumb-config';

export * from './breadcrumb.service';
export * from './breadcrumb.guard';
export * from './breadcrumb-config';
export * from './helpers/index';

@NgModule({
  imports: [RouterModule]
})
export class BreadcrumbModule {

  static forRoot(breadcrumbConfig?: BreadcrumbConfig): ModuleWithProviders {
    return {
      ngModule: BreadcrumbModule,
      providers: [
        BreadcrumbService,
        BreadcrumbGuard,
        { provide: BREADCRUMB_CONFIG, useValue: breadcrumbConfig }
      ]
    };
  }

  constructor( @Optional() @SkipSelf() parentModule: BreadcrumbModule) {
    if (parentModule) {
      throw new Error(
        'BreadcrumbModule is already loaded. Import it only once!');
    }
  }

}
